var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

//TODO: Change conf prop
var confProp = {
    moduleName: 'chiptuning',
    moduleShortName: 'ct'
};

//TODO: JS Conf
var jsCustomProp = {
    base: {
        frameworks: [

        ],
        files: [
            'app/services/AppService.js',
            'app/services/LayoutService.js',
            'app/services/QuickNavService.js',
            'app/services/QuickSidebarService.js',
            'app/services/CustomService.js'
        ]
    },
    modules: [
        {
            fileName: 'main',
            files: []
        }
    ]
};

//TODO: Change other css files
var otherCssProp = {
    filesToCopy: [
        {watch: '**/*.otf', webapp: './../' + confProp.moduleName + '/src/main/webapp'},
        {watch: '**/*.eot', webapp: './../' + confProp.moduleName + '/src/main/webapp'},
        {watch: '**/*.svg', webapp: './../' + confProp.moduleName + '/src/main/webapp'},
        {watch: '**/*.ttf', webapp: './../' + confProp.moduleName + '/src/main/webapp'},
        {watch: '**/*.woff', webapp: './../' + confProp.moduleName + '/src/main/webapp'},
        {watch: '**/*.woff2', webapp: './../' + confProp.moduleName + '/src/main/webapp'}
    ],
    otherFunctions: function () {
        gulp.src('static/' + confProp.moduleShortName + '/sass/frameworks/fonts/**')
            .pipe(gulp.dest('./../' + confProp.moduleName + '/src/main/webapp/static/' + confProp.moduleShortName + '/css/frameworks/fonts/'));
    }
};

/****** SASS *******/
var sassPath = {
    watch: 'static/' + confProp.moduleShortName + '/sass/**/*.scss',
    maps: './maps',
    webappCss: './../' + confProp.moduleName + '/src/main/webapp/static/' + confProp.moduleShortName + '/css'
};
gulp.task('sass', function () {
    gulp.src(sassPath.watch)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write(sassPath.maps))
        .pipe(gulp.dest(sassPath.webappCss));
});

/****** OTHER CSS/FILES *******/
gulp.task('otherCss', function () {
    for (var i = 0; i < otherCssProp.filesToCopy.length; i++) {
        gulp.src(otherCssProp.filesToCopy[i].watch)
            .pipe(gulp.dest(otherCssProp.filesToCopy[i].webapp));
    }

    otherCssProp.otherFunctions();
});

/******** HTML **********/
var htmlProp = {
    js: {
        watch: 'static/' + confProp.moduleShortName + '/js/**/*.html',
        webapp: './../' + confProp.moduleName + '/src/main/webapp/static/' + confProp.moduleShortName + '/dist'
    }
};
gulp.task('html', function () {
    gulp.src(htmlProp.js.watch)
        .pipe(gulp.dest(htmlProp.js.webapp));

});

/*********** IMAGES ***********/
var imagesProp = {
    watch: [
        'static/' + confProp.moduleShortName + '/img/**/*.jpg',
        'static/' + confProp.moduleShortName + '/img/**/*.png',
        'static/' + confProp.moduleShortName + '/img/**/*.jpeg',
        'static/' + confProp.moduleShortName + '/img/**/*.gif'],
    webapp: './../' + confProp.moduleName + '/src/main/webapp/static/' + confProp.moduleShortName + '/img'
};
gulp.task('img', function () {
    gulp.src(imagesProp.watch)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(imagesProp.webapp));
});

/************ JS ***************/
var jsProp = {
    watch: 'static/' + confProp.moduleShortName + '/js/**/*.js',
    webapp: './../' + confProp.moduleName + '/src/main/webapp/static/' + confProp.moduleShortName + '/dist'
};

gulp.task('js', function () {
    for (var i = 0; i < jsCustomProp.modules.length; i++) {
        var wl = [];

        for (var wIndex = 0; wIndex < jsCustomProp.base.frameworks.length; wIndex++) {
            wl.push('static/' + confProp.moduleShortName + '/js/' + jsCustomProp.base.frameworks[wIndex]);
        }
        for (var wIndex = 0; wIndex < jsCustomProp.base.files.length; wIndex++) {
            wl.push('static/' + confProp.moduleShortName + '/js/' + jsCustomProp.base.files[wIndex]);
        }
        for (var wIndex = 0; wIndex < jsCustomProp.modules[i].files.length; wIndex++) {
            wl.push('static/' + confProp.moduleShortName + '/js/' + jsCustomProp.modules[i].files[wIndex]);
        }

        gulp.src(wl)
            .pipe(sourcemaps.init())
            .pipe(uglify())
            .pipe(concat(jsCustomProp.modules[i].fileName + '.js'))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(jsProp.webapp));
    }
});

/************ WATCH TASK **************/
var watchProp = {
    watch: ['sass', 'html', 'js', 'otherCss'],
    watches: [
        {watch: 'static/' + confProp.moduleShortName + '/sass/**/*.scss', func: ['sass']},
        {watch: 'static/' + confProp.moduleShortName + '/js/**/*.html', func: ['html']},
        {watch: 'static/' + confProp.moduleShortName + '/js/**/*.js', func: ['js']},
        {watch: 'static/' + confProp.moduleShortName + '/sass/normalize.css', func: ['otherCss']},
    ]
};
gulp.task('default', watchProp.watch, function () {
    for (var i = 0; i < watchProp.watches.length; i++) {
        gulp.watch(watchProp.watches[i].watch, watchProp.watches[i].func);
    }
});